FROM ruby:2.3-stretch

LABEL Maintainer="Noosfero Development Team <noosfero-dev@listas.softwarelivre.org>"
LABEL Description="This dockerfile builds a noosfero production environment."

ENV RAILS_ENV=production

EXPOSE 3000

RUN apt-get update && apt-get install -y sudo cron nodejs postgresql-client po4a

COPY Gemfile* /tmp/
WORKDIR /tmp
RUN bundle install -j $(nproc) --without development test cucumber

WORKDIR /noosfero
ADD . /noosfero/
COPY config/database.yml.docker config/database.yml

RUN mkdir tmp
RUN bundle exec rake assets:precompile
RUN bundle exec rake noosfero:doc:build

ENTRYPOINT ["config/docker/noosfero-entrypoint.sh"]
CMD ["script/production", "run"]
